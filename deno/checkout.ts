import axiod from "https://deno.land/x/axiod/mod.ts";
import moment from "https://deno.land/x/momentjs@2.29.1-deno/node_modules/moment/moment.js";

const LOTE_INSTALACAO = "1122";

const currentTimestamp = () => moment().utc().unix();
const nextTimestamp = () => moment().add(15, "minutes").utc().unix();
const previousTimestamp = () => moment().subtract(15, "minutes").utc().unix();

function getValidTicket() {
  return `
  <readTicketData>
    <iDataHoraAValidar>${nextTimestamp()}</iDataHoraAValidar>
    <iDataHoraEntrada>${previousTimestamp()}</iDataHoraEntrada>
    <iDataHoraMensagem>${currentTimestamp()}</iDataHoraMensagem>
    <iDataHoraValidada>${currentTimestamp()}</iDataHoraValidada>
    <iDescontoAcumulado>0</iDescontoAcumulado>
    <iEquipEntrada>${LOTE_INSTALACAO}</iEquipEntrada>
    <iLoteInstalacao>${LOTE_INSTALACAO}</iLoteInstalacao>
    <iSequencialControle>0</iSequencialControle>
    <iStatusMensagem>0</iStatusMensagem>
    <iSubLoteInstalacao>${LOTE_INSTALACAO}</iSubLoteInstalacao>
    <iTabelaCobranca>1</iTabelaCobranca>
    <iTipoMensagem>0</iTipoMensagem>
    <iValorAPagar>500</iValorAPagar>
    <iValorPago>0</iValorPago>
    <sIdCartao>${4}</sIdCartao>
  </readTicketData>`;
}

function getPayedTicket() {
  return `
  <readTicketData>
    <iDataHoraAValidar>${nextTimestamp()}</iDataHoraAValidar>
    <iDataHoraEntrada>${previousTimestamp()}</iDataHoraEntrada>
    <iDataHoraMensagem>${currentTimestamp()}</iDataHoraMensagem>
    <iDataHoraValidada>${currentTimestamp()}</iDataHoraValidada>
    <iDescontoAcumulado>0</iDescontoAcumulado>
    <iEquipEntrada>${LOTE_INSTALACAO}</iEquipEntrada>
    <iLoteInstalacao>${LOTE_INSTALACAO}</iLoteInstalacao>
    <iSequencialControle>0</iSequencialControle>
    <iStatusMensagem>0</iStatusMensagem>
    <iSubLoteInstalacao>${LOTE_INSTALACAO}</iSubLoteInstalacao>
    <iTabelaCobranca>1</iTabelaCobranca>
    <iTipoMensagem>0</iTipoMensagem>
    <iValorAPagar>500</iValorAPagar>
    <iValorPago>500</iValorPago>
    <sIdCartao>013391</sIdCartao>
  </readTicketData>`;
}

function getCheckoutTicket() {
  return `
  <setTicketData>
    <iDataHoraAValidar>${nextTimestamp()}</iDataHoraAValidar>
    <iDataHoraEntrada>${previousTimestamp()}</iDataHoraEntrada>
    <iDataHoraMensagem>${currentTimestamp()}</iDataHoraMensagem>
    <iDataHoraValidada>${nextTimestamp()}</iDataHoraValidada>
    <iEquipamento>203</iEquipamento>
    <iLoteInstalacao>159</iLoteInstalacao>
    <iRPS>660</iRPS>
    <iRPSControle>660</iRPSControle>
    <iSequencialMensagem>0</iSequencialMensagem>
    <iStatusMensagem>0</iStatusMensagem>
    <iTabelaCobranca>1</iTabelaCobranca>
    <iTipoMensagem>0</iTipoMensagem>
    <sDescDataHoraValidada>${nextTimestamp()}</sDescDataHoraValidada>
    <sDescStatusMensagem></sDescStatusMensagem>
    <sIdCartao>013391</sIdCartao>
    <sLocalizacao></sLocalizacao>
    <sNumeroCartao>707070</sNumeroCartao>
    <sSerieRPS>00203</sSerieRPS>
    <sValor>500</sValor>
  </setTicketData>
  `;
}

async function handleRequest(url: string) {
  if (url.includes("EfetuarPagamento")) {
    if (url.includes("707070")) {
      return getCheckoutTicket();
    }
    return (await axiod.get(`http://vpn2.nepos.com.br:6050${url}`))?.data;
  }

  if (url.includes("VerDadosPagamento")) {
    if (url.includes("707070")) {
      return getValidTicket();
    }
    if (url.includes("606060")) {
      return getPayedTicket();
    }
    return (await axiod.get(`http://vpn2.nepos.com.br:6050${url}`))?.data;
  }

  return "";
}

function getPath(request: any) {
  const host = request.headers.get("host");
  const url = request.url;

  return url.split(host)[1];
}

addEventListener("fetch", async (event) => {
  let data = "";
  try {
    data = await handleRequest(getPath(event.request));
  } catch (error) {
    console.log(error);
  }

  const response = new Response(data, {
    headers: { "content-type": "application/xml" },
  });
  event.respondWith(response);
});
