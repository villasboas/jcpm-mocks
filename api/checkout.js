const axios = require("axios");
const moment = require("moment");

const LOTE_INSTALACAO = "1122";

const currentTimestamp = () => moment().utc().unix();
const nextTimestamp = () => moment().add(15, "minutes").utc().unix();
const previousTimestamp = () => moment().subtract(15, "minutes").utc().unix();

function getValidTicket() {
  return `
  <readTicketData>
    <iDataHoraAValidar>${nextTimestamp()}</iDataHoraAValidar>
    <iDataHoraEntrada>${previousTimestamp()}</iDataHoraEntrada>
    <iDataHoraMensagem>${currentTimestamp()}</iDataHoraMensagem>
    <iDataHoraValidada>${currentTimestamp()}</iDataHoraValidada>
    <iDescontoAcumulado>0</iDescontoAcumulado>
    <iEquipEntrada>${LOTE_INSTALACAO}</iEquipEntrada>
    <iLoteInstalacao>${LOTE_INSTALACAO}</iLoteInstalacao>
    <iSequencialControle>0</iSequencialControle>
    <iStatusMensagem>0</iStatusMensagem>
    <iSubLoteInstalacao>${LOTE_INSTALACAO}</iSubLoteInstalacao>
    <iTabelaCobranca>1</iTabelaCobranca>
    <iTipoMensagem>0</iTipoMensagem>
    <iValorAPagar>500</iValorAPagar>
    <iValorPago>0</iValorPago>
  </readTicketData>`;
}

function getPayedTicket() {
  return `
  <readTicketData>
    <iDataHoraAValidar>${nextTimestamp()}</iDataHoraAValidar>
    <iDataHoraEntrada>${previousTimestamp()}</iDataHoraEntrada>
    <iDataHoraMensagem>${currentTimestamp()}</iDataHoraMensagem>
    <iDataHoraValidada>${currentTimestamp()}</iDataHoraValidada>
    <iDescontoAcumulado>0</iDescontoAcumulado>
    <iEquipEntrada>${LOTE_INSTALACAO}</iEquipEntrada>
    <iLoteInstalacao>${LOTE_INSTALACAO}</iLoteInstalacao>
    <iSequencialControle>0</iSequencialControle>
    <iStatusMensagem>0</iStatusMensagem>
    <iSubLoteInstalacao>${LOTE_INSTALACAO}</iSubLoteInstalacao>
    <iTabelaCobranca>1</iTabelaCobranca>
    <iTipoMensagem>0</iTipoMensagem>
    <iValorAPagar>500</iValorAPagar>
    <iValorPago>500</iValorPago>
  </readTicketData>`;
}

module.exports = async function (req, res) {
  const { code, a, b, c, d, e, f, g, h, i, j, k, l } = req.query;

  let data = "";
  if (code === "707070") {
    data = getValidTicket();
  } else if (code === "606060") {
    data = getPayedTicket();
  } else {
    data = (
      await axios.get(
        `http://vpn2.nepos.com.br:6050/PagamentoWeb/VerDadosPagamento/${code}/${a}/${b}/${c}/${d}/${e}/${f}/${g}/${h}/${i}/${j}/${k}/${l}`
      )
    )?.data;
  }

  res.setHeader("Content-Type", "application/xml").send(data);
};
