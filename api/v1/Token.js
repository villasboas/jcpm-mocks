import moment from "moment";
import { v4 } from "uuid";

module.exports = (req, res) => {
  res.send({
    Alias: req.body.Alias,
    TokenReference: v4(),
    ExpirationDate: moment().add(1, "year").format("YYYY-MM-DD"),
    Card: {
      Number: `************${req.body.Card.Number.substr(
        req.body.Card.Number.length - 4
      )}`,
      ExpirationDate: req.body.Card.ExpirationDate,
      Holder: req.body.Card.Holder,
      SecurityCode: "***",
    },
    Links: [
      {
        Method: "GET",
        Rel: "self",
        HRef: "https://cartaoprotegidoapisandbox.braspag.com.br/v1/Token/c2e0d46e-6a78-409b-9ad4-75bcb3985762",
      },
      {
        Method: "DELETE",
        Rel: "remove",
        HRef: "https://cartaoprotegidoapisandbox.braspag.com.br/v1/Token/c2e0d46e-6a78-409b-9ad4-75bcb3985762",
      },
      {
        Method: "PUT",
        Rel: "suspend",
        HRef: "https://cartaoprotegidoapisandbox.braspag.com.br/v1/Token/c2e0d46e-6a78-409b-9ad4-75bcb3985762/suspend",
      },
    ],
  });
};
