module.exports = (req, res) => {
  res.send({
    Status: 10,
    ReasonCode: 0,
    ReasonMessage: "Successful",
    ProviderReturnCode: "9",
    ProviderReturnMessage: "Operation Successful",
    Links: [
      {
        Method: "GET",
        Rel: "self",
        Href: "https://apiquerysandbox.braspag.com.br/v2/sales/{PaymentId}",
      },
    ],
  });
};
