import moment from "moment";
import { v4 } from "uuid";

module.exports = (req, res) => {
  res.send({
    MerchantOrderId: "211111111111",
    Payment: {
      ServiceTaxAmount: 0,
      Installments: 1,
      Interest: "ByMerchant",
      Capture: true,
      Authenticate: false,
      Recurrent: false,
      ProofOfSale: "515268",
      AcquirerTransactionId: "0826105255063",
      AuthorizationCode: "567914",
      SentOrderId: "211111111111",
      PaymentId: v4(),
      Type: "Pix",
      Amount: 1000,
      ReceivedDate: moment().format("YYYY-MM-DD hh:mm:ss"),
      CapturedAmount: 1000,
      CapturedDate: moment().format("YYYY-MM-DD hh:mm:ss"),
      Currency: "BRL",
      Country: "BRA",
      Provider: "Simulado",
      ReasonCode: 0,
      ReasonMessage: "Successful",
      Status: 2,
      ProviderReturnCode: "6",
      ProviderReturnMessage: "Operation Successful",
      Links: [
        {
          Method: "GET",
          Rel: "self",
          Href: "https://apiquerysandbox.braspag.com.br/v2/sales/871aa028-c164-4ded-a319-7b7c56dd3061",
        },
        {
          Method: "PUT",
          Rel: "void",
          Href: "https://apisandbox.braspag.com.br/v2/sales/871aa028-c164-4ded-a319-7b7c56dd3061/void",
        },
      ],
    },
  });
};
