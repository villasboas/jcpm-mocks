import axios from "axios";
import moment from "moment";
import { v4 } from "uuid";
module.exports = (req, res) => {
  if (req.body.Payment.Type === "Pix") {
    const paymentId = v4();
    res.send(
      {
        "MerchantOrderId":"2020102601",
        "Customer":{
           "Name":"Nome do Pagador"
        },
        "Payment":{
           "PaymentId": paymentId,
           "Type":"Pix",
           "Provider":"Cielo30",
           "AcquirerTransactionId":"86c200c7-7cdf-4375-92dd-1f62dfa846ad",
              "ProofOfSale":"123456",
           "QrcodeBase64Image":"iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAAAXNSR0IArs4c6QAADaRJREFUeF7tnf+V5FYRhcsRmAzWGwF2BIsj2XUEQAQLERgi8DoSQwSGCLAzgAjMqeke1EPLel/NlKTW9DfnzF+qrle6r27d90NP+iwifgn/EoG/R8TvmqBIPz8AX19HxN8GdtQXaA7fY8b0jjh87TafSZD/dbEEmbJdgpyxkCBTUkgQCXIliBJEgsyNklQQFeQqL1QQFUQFWZhUShAJIkEkCFqpc4jlEMsh1kKxkCASRIJIkPEuDl3FyvH5aENr3Np+Fh8i4s2geToHIb7eRsR7cLudG4XfR8RPgzbz+icQF1GQn6Ev0NwuJrkBO9wMpQT5c0T8aZfb6GmUdDglCPFFo+4kCPFF4yL3SPGibW5tl/n8cdSoBKnvg5DkGeH+eJ0kNX3UhPiicZF7lCAXaKog6yyBkqSWIJTWNTsV5AKvzopIfNGukiAUqX47CSJBnp1VpAg4xHKINZtgJHloZqogFKl+OxVEBXl2VpEioIKoICrIAsUkiASRIBKEHbmly7xbH9+lcXUOGYgvOrYhcxDqi8RFq36nLzTWpzcJ7XJ/b/SH4ureKJQgo255el2C1PCi1hLkjJQKMqVMZ9Xv9IUqNc18aCdBJMhVqnQmdacvCXLRVQ6xYIk7mznEquFFrVUQFUQFWWCLBJEgEkSCjAXVSbqT9LksUUFUEBVEBblNBckjt18MQsvrXUduqS9SNTuP3NJNR1ex7mwVa0zb00uwt3559a2uiEkQCXLFGQkyQSJBJIgEWZBVCSJBJIgEYa/9uYeddOcgJwScpF9kAt1vkCAn0JyDOAeZLaQSRIL8f2I4B3EO4hzEOYhzEDL3eLRxiOUQ626HWJ1nJSqkG9l2xtXpyyHWnQ2xOpNnlPSV651xdfqSIBLkKo/pEmiFACPbzqTu9CVBJIgEcZLuJP0xBzqr60gVKtc74+r0pYKoICqICqKCqCBPWUDnWSqICqKCqCAqiAqigvxqHaAPK279oc+cTJKv73ZOOjt9kUk4PXL7TeOXfDvvMZ8qyP8t/0geoqFf97t5twSh0lZnh3f6IvdAH1shvui8Yet7JLF320iQC0Q7O7zTF+l0CUJQqttIEAny7MWDrYtAPb1f/gsJIkEkyEtX15yDTAje6vjcIdbL1WLOgwqigqggKgirLp1j6k5fJHoVhKBUt1FBVBAVRAVhlaOz6nf6ItGrIASluk2rgvwcEf+qxzD7C7ow0NTcg5uvIuLzgcM9Jul/iYjfDuJK7D8BMIivznv8T0T8COLq7O9OX2/BkwfR2SDA6qZNOpPnHnzddGd2BSdB9l3m7Ryube2rKwdv2o8EkSBzCUrIdtOJ3RWcBJEgEmSBTRJEgkgQCYIE9x4m1p33iEA9upEKooKoICoIqmOd1fUefCFQj26UCkKOrB79Pkn8/4iIPwBDssLzz4j4PfD1V7BR2OmLEjc3Hb8E8b96E/Lp4FcPQvEGCUGKLjczpwTZLKBbb0iC1HtIgtQxO+wvJEi96yRIHbPD/kKC1LtOgtQxO+wvJEi96yRIHbPD/kKC1LtOgtQxO+wvJEi96yRIHbPD/kKC1LtOgtQxO+wvJEi96yRIHbPD/iIJ8gOInj6z9TXw9SEi3gM74gu4eTAhx1HpPX4fET8NGs5d6G9pcAO73EknO/ykuX9HRD4xMPrrxGvU1uN10t80d0ib2Y/Do8w0KUiDaUMUCR2Wh75oXJ1VPzty9HjOHi9aoFgQu068SHvduUPaRF8skCAEyqc2EqSOGflFZ3El7UmQC5Q6K6IEIelXt5EgZ8wcYtWSZ48HDDsLCr1bCSJBaK48sZMgExy0uBKgHWI5xCJ5MmujgpxhcZJezyHnIHXMyC8cYjnEInlyZeMQa+ch1mhNP8PLL62+Ad1LqkBu9uT/6I98GZXGlV+ATdulv9+A46/5exXkhGK+m5dsOo76+fE66W+aO+9Ao7lR+N3IjiR0+qCTI+pvFBe9TuPqTOpOX+Q+b1VB9oiL4JU2v1DDkR1NaJqI1N8oLnqdxtWZ1J2+yH3ukYhkkr5HXAQvCXKBkgShKVOzkyBnvGjFp4lI/dW669etaVydVb/TF8Fhj0otQSTIVW7SBwwlyAm6PYhLCopDLIdYNE+ebaeCqCAqyAJ9JIgEkSASZKywdFJNJ8PU3zgyZkHj6pw3dPoid7nHWF8FWUlByK48OupIMqewgZk7vnncdOmPfk02d3NHTxVQX+RoKyUI8UWfvSPHiunxXdiVrUe/80nd0R86vksrPq3Uo6DyOnrMmDgqEIS4o4nYWV239kVwSBuiktQXtWvb/YbHtVFOS5Cp+yTIhIUEWWmIRaqFCjKhpIJMWKggZywkiASZK6QSRIJc5YUKooJcJYUKooKoIAuTEQkiQSSIBAlyQs4hlkOsK6qgo47np0VHq2JoLTsi8v22o43Czq/cdi4Z06/ckmPFIzwfr+e7gDuP05J2ySQ9N2BH70XOtj6CBhOv4Tui99gHAbE/mJDYKEE61/Wt+rQHa3aEIHR4Tnyh6EgSpiOaiKhRaERio3FJEAj6jmYkqSXIRQdJkFq2dhaBWss91hKkiKMEqQEmQVaY8JMkdIj1NFGdg9SIS61VEIrU2Y6Q1znIBKoKooJcUUyCSJC5ukvUCNVrUqUdYjnEQsn0QiOS1K5iuYr17DRziLXSEIt85fYtOGaa4XV+qZQc380da2JHjsnS46hfRcTngzTOFzv/CFKdHG0Fbh5MyD3SL+aS47vUFzraGoEezdlFQYi00U4iQzY6byBtUsDIyhNpr9ums+qTe+x8BIb62qO/23KaVk2aGBKEInWykyA1vGhBlCBnXClgpLrWuqrHWoLUcKT9LUEkyFVmkSJAh0WdvhxirbTyRGoLrSikw0l73TYqSA1R2t8qiAqigixwS4JIEAkiQcbySyXXIdYJS+cg45x6YpHLsmSjjbol57Dpl0pJm58iIv9Hf7nx9eXICF5PP6ONQugKLfPSr++SI7f0WDHBK4++Dr8SGxHoaCsEjBbE1iEWjE2zMwKdakQm6Z1fvursRBpXZ5sSpBPNlXxJkBOwEmSlBDu6WwkiQY6ew6vGL0EkyKoJdnTnEkSCHD2HV41fgkiQVRPs6M4liAQ5eg6vGr8EkSCrJtjRnUuQOyMIOXLbebBqD1+kTXqEdGuC5E46eQqAfMk3/XwLKtQfwcur6T5I51eNc/eevLya7KSjuEjiADxfhUnnc0oUELKTTn0RO5rUJC7qi+5+k/ipDSEIikuCTJBLkAkLCXLGQoJIkLmqLEEkyFVeqCAqyFVSqCAqiAqyMLORIBJEgkgQtPjhEMshlkOsBapIEAnybILQr4uiUr2DETkmSwlCjqPSY7JktYj6IsPlfK8w2SgkcdF9kM6vGtPUIcfI0XFtAmoGhTZVaPQ72JHdb0oQEj5Nns5EJHFRmz3iIq+tpfG32UmQ+iSdgC9BCEpPbSRIHbO2X6ggNShVkDNeKogKsvYyL6WmCkKRWsFOBamBqoKoIFcZ4yR9nWVeSk0VhCK1gp0KUgNVBVFBVJAFzkgQCSJBJMhYVrtXsT6OmwzaJnD18LZysmu69RDri/NXZ0f3kLu5oyOkdE+l84u5GffoVF5++fj96AYL1/NLVKM/2t8jP3k9cX03MqTJSnfSR6CO4qlep3FtTZDqfSzZU4KQYRGNi+BFfXXa0f4mbaJPw0mQCcrOVSzSQdRGgkxISRCaNWc7ChipiBJkAp/gVeyqFnPa36QxFeQCJdLhEkSCXBHLIZZDrLlqSwoKqdLdNipIEVEKGOlwFUQFUUEWCChBJIgEkSBIo4niIkfNRnTEQJp1ku4kneTJrI0EOcPiJL0+Sb/V5OncKCTMovsztOqTTeZOX+Qe8WMfmweGoudn5UlS0zkI8QXDbzWTIBOchGwIfBVEBUGJMmOkglyAooLc/gqPCqKCXNUxSlwyLHKIVdMSFUQFOdQKjwqigqggC0VegkgQCSJBghyschXrnCjOQSbGqCAqyE0oyIeIyCO1S395ves4ar44PI/mkr9R5az4GrVHjxWnn1FcaUNf+9PlyyO3Fz3cuYo1Spy8Tld4iK/O1TXqi8RFbdAzT5Agm/tyo3Dq5s7kkSATrpsndSfZJIgEoUrwXDsJcoEcGR8+F+i53+0xSSfxqyAqyGyeSJATLBJEgkiQBSmRIBJEgkgQMtp82Nijb9wcOdzcl5N0J+mjpHzp9c2T2lWsqcucpE9YbL3XQ4kjQShSO9p1Js+t+iLw7jE36iQIucdWm+4hVmtwjc5uNak74yJwSRCC0oWNBKnPQTqTutMX6XoJQlCSILMo0UdNOpO60xfpeglCUJIgEqSYJ3PmtKA4B2kAe20XnZX6Vn0RDFUQgpIKooIU80QFGQBG9xsacF/Fxa1W/c64CHAqCEFJBVFBinmiggwAy0pHviZLcKdLy52+vomINwOHdNLZWfWJrzwm+x0Ag+BKj7WC5h5MyNPbeVZ++DXZCPQyBvqVW/LsF/JFQKVgHd3uVgnSiSu9R9Jm53CNtJc2dKhPiIt8SZCpa2jykKrf6YsmD7GjcRFfEoSg9IpsaPJIkFOnS5BXlPzkViQIQWmykSA1vA5vLUFqXShBangd3lqC1LpQgtTwOry1BKl1oQSp4XV4awlS60IJUsPr8NYSpNaFd0GQ/wKcc2lyedTSgQAAAABJRU5ErkJggg==",
           "QrCodeString":"00020101021226880014br.gov.bcb.pix2566qrcodes-h.cielo.com.br/pix-qr/d05b1a34-ec52-4201-ba1e-d3cc2a43162552040000530398654041.005802BR5918Merchant Teste HML6009Sao Paulo62120508000101296304031C",
           "Amount": req.body.Payment.Amount,
           "ReceivedDate":"2020-10-15 18:53:20",
           "Status":12,
           "ProviderReturnCode":"0",
           "ProviderReturnMessage":"Pix gerado com sucesso",
        }
    })
    const url = req.url
    setTimeout(async () => {
      await axios.post(`${url}/payment-webhook`, { PaymentId: paymentId, ChangeType: '1' })
    }, 2000)
    return
  }
  res.send({
    MerchantOrderId: "211111111111",
    Customer: {
      Name: req.body.Customer.Name,
      Identity: req.body.Customer.Identity,
      IdentityType: req.body.Customer.IdentityType,
    },
    Payment: {
      ServiceTaxAmount: 0,
      Installments: 1,
      Interest: "ByMerchant",
      Capture: true,
      Authenticate: false,
      Recurrent: false,
      CreditCard: {
        CardNumber: req.body?.Payment?.CreditCard?.CardNumber,
        Holder: req.body.CreditCard?.Payment?.Holder,
        SaveCard: req.body.CreditCard?.Payment?.SaveCard,
        Brand: req.body?.CreditCard?.Payment?.Brand,
      },
      ProofOfSale: "515268",
      AcquirerTransactionId: "0826105255063",
      AuthorizationCode: "567914",
      SentOrderId: "211111111111",
      PaymentId: v4(),
      Type: "CreditCard",
      Amount: req.body.PaymentAmount,
      ReceivedDate: moment().format("YYYY-MM-DD hh:mm:ss"),
      CapturedAmount: req?.body?.Payment?.Amount,
      CapturedDate: moment().format("YYYY-MM-DD hh:mm:ss"),
      Currency: "BRL",
      Country: "BRA",
      Provider: "Simulado",
      ExtraDataCollection: req.body.Payment.ExtraDataCollection,
      ReasonCode: 0,
      ReasonMessage: "Successful",
      Status: 2,
      ProviderReturnCode: "6",
      ProviderReturnMessage: "Operation Successful",
      Links: [
        {
          Method: "GET",
          Rel: "self",
          Href: "https://apiquerysandbox.braspag.com.br/v2/sales/871aa028-c164-4ded-a319-7b7c56dd3061",
        },
        {
          Method: "PUT",
          Rel: "void",
          Href: "https://apisandbox.braspag.com.br/v2/sales/871aa028-c164-4ded-a319-7b7c56dd3061/void",
        },
      ],
    },
  });
};
