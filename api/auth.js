module.exports = (req, res) => {
  if (req.headers["authorization"] === "Bearer YXBpOnBhc3N3b3Jk") {
    res.json({
      email: "mock@gmail.com",
      name: "Mock User",
      id: "44589",
    });
    return;
  }
  res
    .json({
      body: "Acesso negado",
    })
    .status(401);
};
